Reactive Cart 1.0 for Deloitte by David Ball
============================================

Responsive shopping cart using React and MobX

![Screenshot](http://i.imgur.com/3mnuQnt.png)

Code Overview
-------------

This project makes use of React's effecient live DOM updates and MOBX's concept of storing application state in a datastore and modfying it using actions. The Jest test framework is used for testing. The Project uses the Javascript ES5 and Babel as a compiler. Commands are provided to built and run.

The use of React means whenever the data changes the components will re-render in the browser. This makes logic, data and UI consistant which reduces complexity and makes the application easier to extend.

Testing is performed on the data / logic of the datastore as well as the the user interface components.

UI Code can be found in src/components, datastore and and front end business logic (actions) can be found in src/data.

Requirements
------------

Requires Nodejs Installed before running http://node.js


Installation
------------

in project root (where package.json is) run the following commands:
~~~~
npm install
npm start
~~~~

This should automatically build the project and launch a browser with the app loaded.


Voucher Codes
------------

* Use code **"SAVE5"** for £5.00 off your order. 
* Use code **"SAVE10"** for £10.00 off when you spend over £50.00. 
* Use code **"SAVE15"** for £15.00 off when you spend over £75.00 and have at least one
footwear item in the cart.


Running Tests
-------------

To run the automated unit tests run the following command:
~~~~
npm test
~~~~

Additional Thoughts
===================

If this was a production application I would provide more functions for datatransfer with the backend server, howerver I understand this is not a requirement. Additionally  I would also use Jest to automate visual testing using it's screenshot comparison features. Testing in general would be more extensive in production.

Additionally, more sophisticated stock handling would be used in a production cart, a consumer would typically not be able to see live stock levels. This is out of the scope of this project.