import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from "./data/DataStore";
import './css/global.css';
import './css/media_queries.css'

import { ProductListDisplay } from "./components/ProductListDisplay";
import { CartListDisplay } from "./components/CartListDisplay";
import { VoucherAreaDisplay } from "./components/VoucherAreaDisplay";
import { ListToggleDisplay } from "./components/ListToggleDisplay";

@observer
class App extends Component {
  constructor() {
    super();
    this.state = {
      catsHidden: true,
    };
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img id="logo" role="presentation" src="/logo.png" onClick={() => store.activeCat = null}/>
          <ListToggleDisplay prodCats={store.catStrings}/>
        </div>
        <div className="cart">
          <CartListDisplay store={store} />
          <VoucherAreaDisplay store="store"/>
        </div>
        <ProductListDisplay store={store} />
      </div>
      );
  }
}

export default App;