import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// import renderer from 'react-test-renderer';

import { store } from "./data/DataStore";
import Product from "./data_structures/Product";

import CartItem from "./data_structures/CartItem";
import ProductDisplay from "./components/ProductDisplay";

/**
 * App.test.js
 * Test framework: https://facebook.github.io/jest/
 * Activate by running "npm test" in project root.
 */

it('page does not crash', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

/**
 * Action testing. Business logic MOBX DataStore.js
 */

test('check store exists', () => {
  let testStore = new store;
  expect(testStore);
});


test('cart totals being calculated correctly', () => {
  let testStore = new store;
  let cartItem = new CartItem(1, "shoes", "blue", 3, 10, 1);
  testStore.addToCart(cartItem);
  expect(testStore.subtotal).toBe(10);
  testStore.addToCart(cartItem);
  expect(testStore.cartTotal).toBe(20);
  testStore.removeFromCart(cartItem);
  testStore.removeFromCart(cartItem);
  expect(testStore.subtotal).toBe(0);
  expect(testStore.cartTotal).toBe(0);
});


test('cart quantities accurate', () => {
	let testStore = new store;
	expect(testStore.getCartNum()).toBe(0);
	let twoItems = new CartItem(1, "shoes", "blue", 2, 200, 2);
	testStore.addToCart(twoItems);
	expect(testStore.getCartNum()).toBe(2);
	let oneItem = new CartItem(1, "shoes", "blue", 2, 200, 1);
	testStore.addToCart(oneItem);
	expect(testStore.getCartNum()).toBe(3);
	testStore.removeFromCart(oneItem); // meant to remove all of this item
	expect(testStore.getCartNum()).toBe(0);
	});

test('rejects invalid vouchers correctly', () => {

	let testStore = new store;
	expect(testStore.cartTotal).toBe(0);

	// wrong voucher cod
	expect(testStore.applyVoucher("")).toBe("Invalid Voucher");
	// voucher exists but cart emppty
	expect(testStore.applyVoucher("SAVE5")).toBe("Cart requirements not met");
	expect(testStore.applyVoucher("SAVE10")).toBe("Cart requirements not met");
	expect(testStore.applyVoucher("SAVE15")).toBe("Cart requirements not met");

	let cartItem = new CartItem(1, "shoes", "blue", 3, 99.99, 1);
  	testStore.addToCart(cartItem);
	// malformed voucher with cart eligable
	expect(testStore.applyVoucher("SA VE5")).toBe("Invalid Voucher");
});

test('accepts valid vouchers without error codes', () => {

	let testStore = new store;
	let cartItem = new CartItem(1, "shoes", "blue", 2, 200, 2);
	testStore.addToCart(cartItem);
	expect(testStore.applyVoucher("SAVE5")).toBe(null);
	expect(testStore.applyVoucher("SAVE10")).toBe(null);
	expect(testStore.applyVoucher("SAVE15")).toBe(null);
});

test('accepts malformed copy pasted vouchers', () => {

	let testStore = new store;
	let cartItem = new CartItem(1, "shoes", "blue", 2, 200, 2);
	testStore.addToCart(cartItem);
	expect(testStore.applyVoucher(" save5  ")).toBe(null);
});


test('vouchers discounts subtracted from order total correctly', () => {
	let testStore = new store;
	let cartItem = new CartItem(1, "shoes", "blue", 2, 200, 2);
	testStore.addToCart(cartItem);
	expect(testStore.applyVoucher("SAVE5")).toBe(null);
	expect(testStore.cartTotal).toBe(395);
	expect(testStore.applyVoucher("SAVE10")).toBe(null);
	expect(testStore.cartTotal).toBe(385);
	expect(testStore.applyVoucher("SAVE15")).toBe(null);
	expect(testStore.cartTotal).toBe(370);
	});

test('items removes from cart correctly', () => {

	let testStore = new store;
	let cartItem = new CartItem(1, "shoes", "blue", 2, 200, 1);
	testStore.addToCart(cartItem);
	expect(testStore.cartTotal).toBe(200);
	testStore.removeFromCart(cartItem);
	expect(testStore.cartTotal).toBe(0);
	});

test('voucher discounts removed progressively with repeated validations', () => {
	let testStore = new store;
	let cartItem = new CartItem(1, "shoes", "blue", 2, 50, 1);
	let cartItem2 = new CartItem(2, "clown shoes", "blue", 2, 50, 1);
	testStore.addToCart(cartItem);
	testStore.addToCart(cartItem2);
	expect(testStore.applyVoucher("SAVE15")).toBe(null);
	expect(testStore.cartTotal).toBe(85);
	testStore.removeFromCart(cartItem);

	expect(testStore.cartTotal).toBe(50);
});