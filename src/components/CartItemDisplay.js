import React, { Component } from 'react';
import { observer } from "mobx-react";

/**
 * React component for displaying a Cart Item
 */
@observer
export class CartItemDisplay extends Component {
  
  render() {
    let item = this.props.cartItem;

    return (
      <div className="cartItem">
        <span className="cartItemName">{item.pName}, {item.pColour}</span>
        <span className="cartItemPrice"> £{item.pPrice}</span>
        <span className="cartItemQuantity"> x{item.pQuantity}</span>
        <span className="cartDelete" onClick={() => item.remove()}>Delete</span>
      </div>
    );
  }
}