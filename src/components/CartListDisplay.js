import React, { Component } from 'react';
import { observer } from "mobx-react";
import store from "../data/DataStore";
import { CartItemDisplay } from "./CartItemDisplay";


/**
 * React Component for the display of the list of cart items
 */
@observer
export class CartListDisplay extends Component {
  render() {
    let numItems = this.props.store.getCartNum();
    let cartMessage = "Cart is Empty";

    if (numItems > 0) {
      cartMessage = numItems += " item" + ((numItems === 1) ? "" : "s") + " in Cart";
    }
      
    this.cartList = this.props.store.cartItems.map((cartItem) => 
    <ul key={cartItem.pId}>
      <CartItemDisplay cartItem={cartItem} store={store}/>
    </ul>
    );

    let voucherSavings = null;
    if (this.props.store.voucherDiscount !== 0) {
      voucherSavings = <span className="voucherSavings cartMoney">Voucher Savings: -£{this.props.store.voucherDiscount} </span>
    }

    return (<div className="cartContainer">
      <div>
      <h2 className="cartCount">{cartMessage}</h2>
      <div className="cartList"> {this.cartList}</div>
      <span className="subTotal cartMoney">Sub Total: £{this.props.store.subtotal} </span>
      {voucherSavings}
      <span className="total cartMoney">Cart Total: £{this.props.store.cartTotal} </span>
      </div>
    </div>);
  }
}