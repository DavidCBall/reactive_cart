import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from "../data/DataStore";

@observer
/**
 * React Component for the display of the department / cateogry drop down list
 */
export class CatListDisplay extends Component {

  closeMenu(error) {
    store.menuOpen = false;
  }

  render() {
    let cats = store.catStrings;
    this.listItems = cats.map(function(cat) {

      let classes = "navItem";

      // add an extra 'active' class
      if (store.activeCat === cats.indexOf(cat)) {
        classes += " active";
      }

      return <a className={ classes } key={ cat.toString() } onClick={ () => store.activeCat = cats.indexOf(cat) }>
               { cat }
             </a>
    });

    if (store.menuOpen) {
      return <nav className="categories" onMouseLeave={ () => this.closeMenu() } onClick={ () => this.closeMenu()}>
               { this.listItems }
             </nav>;
    } else {
      return null;
    }
  }
}