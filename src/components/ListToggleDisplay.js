import React, { Component } from 'react';
import { CatListDisplay } from './CatListDisplay';
import store from "../data/DataStore";

/**
 * React Component for the departments / categorys button, acts as a parent to CatListDisplay
 */

export class ListToggleDisplay extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    if (store.menuOpen === false){
      store.menuOpen = true;
    } else {
      store.menuOpen = false;
    }
  }

  render() {
    return (
      <div>
        <a className="navToggle" onClick={ this.handleClick }>Departments</a>
        <CatListDisplay/>
      </div>
      );
  }
}