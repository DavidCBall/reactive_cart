import React, { Component } from 'react';
import { observer } from "mobx-react"

/**
 * react component for displaying a product
 */
@observer
export class ProductDisplay extends Component {
  render() {

    let product = this.props.prod;
    let button = null;
    let availability = 0;

    let imgUrl = product.pImage;
    let image = <img role="presentation" src={ imgUrl }></img>

    if (product.pQuantity > 0) {
      button = <a className="addToCart" onClick={ () => product.addToCart() }>add to cart</a>
      availability = <span className="productDetail">{ product.pQuantity } left</span>
    } else {
      button = <span className="soldOut">Sold Out</span>
      availability = <span className="productDetail">Out of Stock</span>
    }

    return (
      <div className="product">
        <div className="productControls">
          <span className="productDetail productName">{ product.pName }</span>
          <div>
            <span className="productDetail productColour">{ product.pColour }</span>
            <span className="productDetail productCat">, { this.props.store.getCatString(product.pCat) }</span>
          </div>
          <span className="productDetail productPrice">£{ product.pPrice }</span>
          <span className="productAvailability">{ availability }</span>
        </div>
        <div className="productImage">
          { image }
        </div>
        <div className="addToCartContainer">
          { button }
        </div>
      </div>
    )
  }
}

export default ProductDisplay