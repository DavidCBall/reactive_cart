import React, { Component } from 'react';
import { observer } from "mobx-react"
import { ProductDisplay } from "./ProductDisplay"
import store from "../data/DataStore"

/**
 * React component for building a list of products, acts as a parent to ProductDisplay
 */

@observer
export class ProductListDisplay extends Component {
  render() {

    let productList = this.props.store.products.map(function(prod) {
      if (prod.pCat === store.activeCat || store.activeCat === null) {
        return (
          <li className="productContainer" key={ prod.pId }>
            <ProductDisplay prod={ prod } store={ store } />
          </li>
          );
      } else {
        return null;
      }
    })

    return <ul className="productList">
             { productList }
           </ul>;
  }
}