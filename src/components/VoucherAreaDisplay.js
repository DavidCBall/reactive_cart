import React, { Component } from 'react';
import { observer } from "mobx-react"
import store from "../data/DataStore";

/**
 * React component for the voucher area and form
 */

@observer
export class VoucherAreaDisplay extends Component {
  constructor(props) {
    super(props);
    this.button = null;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      couponCode: "",
      voucherError: ""
    };
  }

  activeVouchers() {
    let voucherText = "";
    for (let voucher in store.vouchers) {
      if (store.vouchers[voucher].active) {
        voucherText += "£" + store.vouchers[voucher].amount + " Off this purchase, ";
      }
    }
    return voucherText;
  }

  handleChange(e) {
    this.setState({
      couponCode: e.target.value
    });
    this.button = <button>
                    { 'apply voucher' }
                  </button>
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({
      couponCode: "",
      voucherError: store.applyVoucher(this.state.couponCode),
    });
  }

  render() {
    this.value = "";
    return (
      <div id="VoucherArea">
        <span className="activeVouchers">{ this.activeVouchers() }</span>
        <form onSubmit={ this.handleSubmit }>
          <div>
            <span>Discount Code</span>
          </div>
          <input onChange={ this.handleChange } value={ this.state.couponCode } />
          { this.button }
          <span className="voucherError">{ this.state.voucherError }</span>
        </form>
      </div>
    )
  }
}