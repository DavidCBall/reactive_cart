import { observable, action } from "mobx";
import Product from "../data_structures/Product"

/**
 * Defines Application state and business logic (actions)
 * Implimentation of the MobX Datastore cocnept: https://mobx.js.org/
 */
export class store {
    constructor() {
    this.initProducts();
  }

  /**
   * Application State
   */

  @observable menuOpen = false;
  @observable activeCat = null;
  @observable products = [];
  @observable cartItems = []
  @observable subtotal = 0;
  @observable cartTotal = 0.0;
  @observable voucherDiscount = 0.0;
  @observable vouchers = {
    fiveOff: {
      active: false,
      amount: 5,
      code: "SAVE5"
    },
    tenOff: {
      active: false,
      amount: 10,
      code: "SAVE10"
    },
    fifteenOff: {
      active: false,
      amount: 15,
      code: "SAVE15"
    }
  }

  @observable catStrings =["Women’s Casualwear", // 0
    "Women’s Formalwear", // 1
    "Women’s Footwear", // 2
    "Men’s Casualwear", // 3
    "Men’s Formalwear", // 4
    "Men’s Footwear"]; // 5

  /**
   * Application Actions
   */

   /**
    * Initalises the mock products
    */
  @action initProducts() {
    // (pID, pName, pColour, pCat, pPrice, pQuantity, pImage)
    this.products.push(new Product(1, "Almond Toe Court Shoes", "Patent Black", 2, 4.00, 5, "/images/1.jpg"));
    this.products.push(new Product(2, "Suede Shoes", "Blue", 2, 42.00, 4, "/images/2.jpg"));
    this.products.push(new Product(3, "Leather Driver Saddle Loafers", "Tan", 5, 34.00, 12, "/images/3.jpg"));
    this.products.push(new Product(4, "Flip Flops", "Red", 5, 19.00, 6, "/images/4.jpg"));
    this.products.push(new Product(5, "Flip Flops", "Blue", 5, 19.00, 0, "/images/5.jpg"));
    this.products.push(new Product(6, "Gold Button Cardigan", "Black", 0, 167.00, 6, "/images/6.jpg"));
    this.products.push(new Product(7, "Cotton Shorts, Medium", "Red", 0, 30.00, 5, "/images/7.jpg"));
    this.products.push(new Product(8, "Fine Stripe Short Sleeve Shirt", "Grey", 3, 49.99, 9, "/images/8.jpg"));
    this.products.push(new Product(9, "Fine Stripe Short Sleeve Shirt", "Green", 3, 39.99, 3, "/images/9.jpg"));
    this.products.push(new Product(10, "Sharkskin Waistcoat", "Charcoal", 4, 75.00, 2, "/images/10.jpg"));
    this.products.push(new Product(11, "Lightweight Patch Pocket Blazer", "Deer", 4, 175.00, 1, "/images/11.jpg"));
    this.products.push(new Product(12, "Bird Print Dress", "Black", 1, 270.00, 10, "/images/12.jpg"));
    this.products.push(new Product(13, "Mid Twist Cut-Out Dress", "Pink", 1, 540.00, 5, "/images/13.jpg"));
  }

  /**
   * Removes Item from cart
   * @param  {CartItem} cartItem The Item in question
   */
  @action removeFromCart(cartItem) {
    let cartPosition = this.posInArray(cartItem.pId, this.cartItems);
    let productListPosition = this.posInArray(cartItem.pId, this.products);
    this.products[productListPosition].increaseQuantity(cartItem.pQuantity);

    this.cartItems.splice(cartPosition, 1);
    this.calculateCartTotal();
  }

  /**
   * calculates a total for the cart
   * @return {double} cart total
   */
  @action calculateCartTotal() {
    let calculation = 0;
    for (let i = 0; i < this.cartItems.length; i++) {
      calculation += (this.cartItems[i].pPrice * this.cartItems[i].pQuantity);
    }
    // this.cartTotal = calculation;
    this.validateVouchers(calculation);

    let savingsCalc = 0;
    for (let v in this.vouchers) {
      if (this.vouchers[v].active) {
        savingsCalc += this.vouchers[v].amount;
      }
    }

    this.voucherDiscount = savingsCalc;
    this.subtotal = calculation;
    this.cartTotal = calculation - savingsCalc;
  }

  /**
   * Checks if the voucher code is valid and applies pending validation
   * @param  {String} rawCode raw code from the text field
   * @return {error} Error message if one exists, can also be null
   */
  @action applyVoucher(rawCode) {
    let code = rawCode.trim().toUpperCase();
    let error = "Invalid Voucher";

    for (let v in this.vouchers) {
      if (this.vouchers[v].code === code) {

        this.vouchers[v].active = true;
        this.validateVouchers(this.subtotal);
        if (this.vouchers[v].active === false) {
          error = "Cart requirements not met";
        } else {
          error = null;
        }
        
      }
    }

    this.calculateCartTotal();

    return (error);
  }

  /**
   * Deactivates vouchers when the cart becomes uneligiable
   * @param  {Double} subtotal cart subtotal
   * @return {undefined}          [description]
   */
  @action validateVouchers(subtotal) {

    if (subtotal < 5) {
      this.vouchers.fiveOff.active = false;
    }
    if (subtotal < 50) {
      this.vouchers.tenOff.active = false;
    }
    if (subtotal < 75 || (!this.isCatPresent(2) && !this.isCatPresent(5))) {
      this.vouchers.fifteenOff.active = false;
    }
  }

  /**
   * Adds a product to cart
   * @param {Product} product Product to be added
   */
  @action addToCart(product) {

    let position = this.posInArray(product.pId, this.cartItems);

    if (position !== null) {
      this.cartItems[position].incriementQuantity();
    } else {
      // not in cart so add it
      this.cartItems.push(product);
    }
    this.calculateCartTotal();
  }

  /**
   * Helper function to get a products position in an array
   * @param  {integer} pId   Product ID
   * @param  {Array of Products i.e the cart
   * @return {integer}       position in array
   */
  @action posInArray(pId, array) {
    let position = null;
    for (let i = 0; i < array.length; i++) {
      if (pId === array[i].pId) {

        position = i;
      }
    }
    return position;
  }

  /**
   * Helper function to get the instance of a product in the product list
   * @param  {integer} pId Product ID
   * @return {Product}     [Instance of Product with the array]
   */
  @action getProduct(pId) {
    let product = "oops";
    for (let i = 0; i < this.products.length; i++) {

      if (pId === this.products[i].pId) {
        product = this.products[i];

      }
    }
    return product;
  }

  /**
   * Gets the number of tiems in the cart
   * @return {integer} Number of items in cart
   */
  @action getCartNum() {
    let num = 0;
    for (let i = 0; i < this.cartItems.length; i++) {
      num += this.cartItems[i].pQuantity;
    }
    return num;
  }

  /**
   * Converts a catogry ID to Category Name
   * @param  {integer} i categoryID 
   * @return {String}  Category Name
   */
  @action getCatString(i) {
    return this.catStrings[i];
  }

  /**
   * determines if a particular category of product present in the cart
   * @param  {integer}  catId CategoryID
   * @return {Boolean}
   */
  @action isCatPresent(catId) {
    let catPresent = false;
    for (let i = 0; i < this.cartItems.length; i++) {
      if (this.cartItems[i].pCat === catId) {
        catPresent = true;
        break;
      }
    }
    return catPresent;
  }
}

export default new store();