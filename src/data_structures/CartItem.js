import {observable} from "mobx";
import store from "../data/DataStore";

class CartItem {

	@observable pName;
	@observable pQuantity;

  /**
   * Constructor for Cart Item
   * @param  {Integer} pId       ProductID
   * @param  {String} pName      Product Name
   * @param  {String} pColour    Product Colour
   * @param  {Integer} pCat      Product Category
   * @param  {Double} pPrice     Product Price
   * @param  {[type]} pQuantity  Quantity of Product in Stock        
   */
  constructor(pId, pName, pColour, pCat, pPrice, pQuantity) {
    this.pId = pId;
    this.pName = pName;
    this.pColour = pColour;
    this.pCat = pCat;
    this.pPrice = pPrice;
    this.pQuantity = pQuantity;
  }

  /**
   * Increases the quantiy of the Item by one (in the cart)
   */
  incriementQuantity(){
  	this.pQuantity++;
  }

  /**
   * Removes all of the Item from the cart
   */
  remove() {
  	store.removeFromCart(this);
  	this.pQuantity--;
  }
}

export default CartItem; 