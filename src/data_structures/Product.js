import {observable} from "mobx";
import store from "../data/DataStore";
import CartItem from "./CartItem"

class Product {

	@observable pName;
	@observable pQuantity;

  /**
   * Constructor for Product
   * @param  {Integer} pId       ProductID
   * @param  {String} pName      Product Name
   * @param  {String} pColour    Product Colour
   * @param  {Integer} pCat      Product CategoryID
   * @param  {Double} pPrice     Product Price
   * @param  {Integer} pQuantity Quantity of this Product Available
   * @param  {String} pImage     URL for Product Image
   */
  constructor(pId, pName, pColour, pCat, pPrice, pQuantity, pImage) {
    this.pId = pId;
    this.pName = pName;
    this.pColour = pColour;
    this.pCat = pCat;
    this.pPrice = pPrice;
    this.pQuantity = pQuantity;
    this.pImage = pImage;
  }

  /**
   * Increase Qualtity of the product available (For product stock display)
   * @param  {Integer} number Amount to increase by
   */
  increaseQuantity(number) {
    this.pQuantity += number;
  }

  /**
   * Asks data store to add one of this product to cart, decrements quantity 
   */
  addToCart() {
  	let catItemToAdd = new CartItem(this.pId, this.pName, this.pColour, this.pCat, this.pPrice, 1)
  	store.addToCart(catItemToAdd);
    // decrement quantity held in "stock"
  	this.pQuantity--;
  }
}

export default Product;